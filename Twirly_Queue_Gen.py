#!/usr/bin/python3

import os, sys, shutil

class Twirly_Queue_Gen():
    def __init__(self):
        self.saneDefaults()

    def saneDefaults(self):
        self.gif_in = 'monty.gif'
        self.gif_out = 'processed.gif'

        self.twirl_viewPort = '512x512-50-50'
        self.twirl_sqSize = '512'

        self.twirl_angle = '15'
        self.twirl_phase = 0
        self.twirl_step = 1
        self.twirl_max = 30
        self.twirl_frame = 0
        self.twirl_maxFrame = 5

    def genQueue(self):
        if (os.path.isdir('./temp/') == True):
            shutil.rmtree('./temp/')
        os.mkdir('./temp/')
        os.mkdir('./temp/source/')

        cmd = 'convert ' + self.gif_in + ' +adjoin ./temp/source/src-%d.gif'
        print ('[SPLITTING UP FRAMES] :: ' + cmd)
        if (os.system(cmd)):
            print ("Error! Could not split up source frames!")
            sys.exit(2)

        sourceDir = './temp/source/'
        self.twirl_maxFrame = len([name for name in os.listdir(sourceDir) if os.path.isfile(os.path.join(sourceDir, name))]) - 1
        print ('    ...found frames 0-' + str(self.twirl_maxFrame) + "\n")

        
        imk_stack = []
        for phase in range (self.twirl_phase, self.twirl_max, self.twirl_step):
            imk_stack.append(self.genCommand(phase, self.twirl_frame))
            self.twirl_frame = self.twirl_frame + 1
            if (self.twirl_frame > self.twirl_maxFrame):
                self.twirl_frame = 0
            
        return imk_stack

    def genCommand(self, currentPhase, currentFrame):
        out_file = './temp/' + self.twirl_angle + '-' + str(currentPhase) + '-' + str(currentFrame) + '.gif'
        imk_cmd = 'convert '
        imk_cmd += './temp/source/src-' + str(currentFrame) + '.gif'
        imk_cmd += ' '
        imk_cmd += '-set option:distort:viewport '
        imk_cmd += "'"
        imk_cmd += self.twirl_viewPort
        imk_cmd += "'"
        imk_cmd += ' '
        imk_cmd += '-virtual-pixel tile -distort Arc '
        imk_cmd += "'"
        imk_cmd += self.twirl_angle
        imk_cmd += ' '
        imk_cmd += str(currentPhase)
        imk_cmd += ' '
        imk_cmd += self.twirl_sqSize
        imk_cmd += "'"
        imk_cmd += ' '
        imk_cmd += '+repage +adjoin '
        imk_cmd += out_file
        print ('[ADDING TO QUEUE] :: ' + imk_cmd)
        return imk_cmd
