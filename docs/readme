TWIRLYGIGGER
===========================================

Takes an animated gif and turns it into pinwheel patterns using ImageMagick's Arc Distortion.

ANGLE specifies the Arc Distortion angle.

PHASE, STEP, and MAX control the pinwheel animation:
       - PHASE is the starting rotation angle in degrees.
       - STEP is added to PHASE every frame to get the next rotation angle.
       - MAX is the maximum PHASE to compute. Once reached, the script terminates.

Twirlygigger will calculate the number of frames in the input GIF and cycle through them in the output animation.

Suggested you tune MAX, STEP, and PHASE to match the number of frames in your input GIF for smooth animation.

SQSIZE, VIEWPORT -- Parameters determine geometry of IM's arc distortion.

THREADS -- Number of threads to spawn for processing frames (default=4)

INPUT  -- Default: monty.gif
OUTPUT -- DEFAULT: processed.gif

Simply run './twirlygigger.py' to use all defaults and generate 'processed.gif'
