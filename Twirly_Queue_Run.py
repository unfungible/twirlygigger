#!/usr/bin/python3

import sys, os, shutil
from multiprocessing import Pool

class Twirly_Queue_Run():
    def __init__(self):
        self.saneDefaults()

    def saneDefaults(self):
        self.queue = []

        self.numThreads = 4
        self.twirl_viewPort = '512x512-50-50'
        self.twirl_sqSize = '512'
        self.twirl_angle = '15'
        self.twirl_phase = 0
        self.twirl_step = 1
        self.twirl_max = 30
        self.twirl_frame = 0
        self.twirl_maxFrame = 5
        self.gif_out = 'processed.gif'

    def processQueue(self, imk_queue):
        self.queue = imk_queue
        pool = Pool(processes=self.numThreads)

        print ("\n" + '[IMAGEMAGICK RETURN CODES] :: ' + str(pool.map(self.processOne, self.queue)))

        self.twirl_frame = 0
        cmd = "convert -coalesce "
        for currentPhase in range (self.twirl_phase, self.twirl_max, self.twirl_step):
            cmd += './temp/' + self.twirl_angle + '-' + str(currentPhase) + '-' + str(self.twirl_frame) + '.gif '
            self.twirl_frame = self.twirl_frame + 1
            if self.twirl_frame > self.twirl_maxFrame:
                self.twirl_frame = 0

        cmd += self.gif_out
        print ("\n" + '[BUILDING FINAL ANIMATION] :: ' + cmd)
        os.system(cmd)
        shutil.rmtree('./temp/')

                

    def processOne(self, cmd):
        #return cmd
        return os.system(cmd)
