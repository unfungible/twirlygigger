#!/usr/bin/python3

import sys, getopt
from Twirly_Queue_Gen import *
from Twirly_Queue_Run import *


def printShortHelp():
    print('twirlygigger.py -i <input_gif> -o <output_gif> -v <viewport_geometry> -q <sqsize> -a <angle> -p <phase> -s <step> -m <max> -t <num_threads>')

def printLongHelp():
    print ('twirlygigger.py')
    print ('    -i <input_gif>,  --ifile=<input_gif>    SOURCE GIF        Default: monty.gif')
    print ('    -o <output_gif>, --ofile=<output_gif>   OUTPUT GIF        Default: processed.gif')
    print ('    -v <viewport>,   --viewport=<viewport>  VIEWPORT GEOMETRY Default: 512x512-50-50')
    print ('    -q <sqsize>,     --sqsize=<sqsize>      SQUARE SIZE       Default: 512')
    print ('    -a <angle>,      --angle=<angle>        DIST. ARC ANGLE   Default: 15')
    print ('    -p <phase>,      --phase=<phase>        STARTING PHASE    Default: 0')
    print ('    -s <step>,       --step=<step>          PHASE STEP        Default: 1')
    print ('    -m <max>,        --max=<max>            PHASE MAX         Default: 30')
    print ('    -t <threads>,    --threads=<threads>    NUMBER OF THREADS Default: 4')

    

def main(argv):
    g = Twirly_Queue_Gen()
    r = Twirly_Queue_Run()

    
    try:
        opts, args = getopt.getopt(argv, "hi:o:v:q:a:p:s:m:t:",
                                   ["ifile=","ofile=","viewport=","sqsize=","angle=","phase=","step=","max=","threads="])
    except getopt.GetoptError:
        printShortHelp()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            printLongHelp()
            sys.exit(0)
        elif opt in ("-i", "--ifile"):
            g.gif_in = arg
        elif opt in ("-o", "--ofile"):
            r.gif_out = arg
            g.gif_out = arg
        elif opt in ("-v", "--viewport"):
            r.twirl_viewPort = arg
            g.twirl_viewPort = arg
        elif opt in ("-q", "--sqsize"):
            g.twirl_sqSize = arg
        elif opt in ("-a", "--angle"):
            r.twirl_angle = str(arg)
            g.twirl_angle = str(arg)
        elif opt in ("-p", "--phase"):
            r.twirl_phase = int(arg)
            g.twirl_phase = int(arg)
        elif opt in ("-s", "--step"):
            r.twirl_step = int(arg)
            g.twirl_step = int(arg)
        elif opt in ("-m", "--max"):
            r.twirl_max = int(arg)
            g.twirl_max = int(arg)
        elif opt in ("-t", "--threads"):
            r.numThreads = int(arg)

    print ("\nTwirlygigger\n")
    print ('    [INFILE] :: ' + g.gif_in)
    print ('    [OUTFILE] :: ' + g.gif_out)
    print ('    [THREADS] :: ' + str(r.numThreads))
    print ('    [VIEWPORT] :: ' + str(g.twirl_viewPort))
    print ('    [SQSIZE] :: ' + str(g.twirl_sqSize))
    print ('    [ARC ANGLE] :: ' + str(g.twirl_angle))
    print ('    [STARTING PHASE] :: ' + str(g.twirl_phase))
    print ('    [PHASE STEP] :: ' + str(g.twirl_step))
    print ('    [PHASE MAX] :: ' + str(g.twirl_max))
    print ("\n\n  Processing...\n")

    imk_queue = g.genQueue()
    r.twirl_maxFrame = g.twirl_maxFrame
    r.processQueue(imk_queue)
    print("\n\nDone!")


if __name__ == "__main__":
    main(sys.argv[1:])
